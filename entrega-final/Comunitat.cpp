#include "Comunitat.h"

Comunitat::Comunitat(MatriuSparse* pMAdj)
{
	m_primComdeltaQ = -1;
	m_Q = 0;
	m_pMAdj = pMAdj;
}

Comunitat::~Comunitat()
{
	m_pMAdj = nullptr;	
}
void Comunitat::clear()
{
	m_pMAdj = nullptr;
	m_deltaQ.clear();
	m_indexComs.clear();
	m_maxDeltaQFil.clear();
	m_primComdeltaQ=-1;
	m_vDendrograms.clear();
	m_k.clear();
	m_A.clear();
	m_hTotal.clear();
	m_Q=0;
	m_M2=0;	
}

void Comunitat::calculaA()
{
    m_A.resize(m_k.size());
    for (int i = 0; i < m_A.size(); i++)
        m_A[i] = (double) m_k[i] / m_M2;

    m_k.clear();
}

void Comunitat::creaIndexComs()
{
    m_primComdeltaQ = 0;

    for (int i = 0; i < m_deltaQ.size(); i++)
        m_indexComs.push_back(std::make_pair(i+1, i-1));
}

void Comunitat::creaDeltaQHeap()
{
    m_pMAdj->creaMaps(m_deltaQ);
    
    m_maxDeltaQFil.resize(m_deltaQ.size());
    m_hTotal.resize(m_deltaQ.size());

    for (int i = 0; i < m_deltaQ.size(); i++) {
        std::pair<int, double> maxDeltaQi = std::make_pair(-1, -2);
        std::map<std::pair<int, int>, double>::iterator itp = m_deltaQ[i].begin();
        while (itp != m_deltaQ[i].end()) {
            int j = itp->first.second;
            itp->second = (1.0/m_M2) - ((m_k[i]*m_k[j]+0.0) / (m_M2*m_M2));
            if (itp->second > maxDeltaQi.second)
                maxDeltaQi = std::make_pair(j, itp->second);
            itp++;
        }

        m_maxDeltaQFil[i] = maxDeltaQi;
        m_hTotal.insert(ElemHeap(maxDeltaQi.second, std::make_pair(i, maxDeltaQi.first)));
    }
}

void Comunitat::fusiona(int i, int j)
{
    double deltaQij = m_deltaQ[i][std::make_pair(i, j)];

    // Declarem vectors pels veins de i (vi), j (vj) i comuns (vij)
    std::vector<int> vi, vj, vij;//, vi2;
    
    for (std::pair<std::pair<int, int>, double> p : m_deltaQ[i])
        vi.push_back(p.first.second);
    
    //vi2 = vi;

    for (std::pair<std::pair<int, int>, double> p : m_deltaQ[j])
        vj.push_back(p.first.second);

    // Calculem els veins en comú i treiem aquests de vi i vj
    std::vector<int>::iterator iti = vi.begin();
    while (iti != vi.end()) {
        std::vector<int>::iterator itj = vj.begin();
        while (itj != vj.end() && *iti >= *itj) {
            if (*iti == *itj) {
                vij.push_back(*iti);
                vi.erase(iti);
                vj.erase(itj);
                iti = vi.begin();
                itj--;
            }
            itj++;
        }
        iti++;
    }

    // Realitzem els cálculs per:
    // els veins en comú
    for (int vei : vij) {
        // Agafem el deltaM de (vei, i) i el sumem al deltaM de (vei, j)
        std::map<std::pair<int, int>, double>::iterator it 
            = m_deltaQ[vei].find(std::make_pair(vei, i));
        double deltaMvi = it->second;

        m_deltaQ[vei][std::make_pair(vei, j)] += deltaMvi;

        // Esborrem la relació de (vei, i) 
        m_deltaQ[vei].erase(it);

        // Sumem tambè al deltaM de (j, vei) el deltaM de (i, vei)
        m_deltaQ[j][std::make_pair(j, vei)]
            += m_deltaQ[i][std::make_pair(i, vei)];
    }

    // els veins només de i
    for (int vei : vi) {
        // Agafem el deltaM de (vei, i)
        std::map<std::pair<int, int>, double>::iterator it 
            = m_deltaQ[vei].find(std::make_pair(vei, i));
        double deltaMvi = it->second;

        // Esborrem la relacio de (vei, i)
        m_deltaQ[vei].erase(it);

        if (vei != j) {
            // Calculem un deltaM per (vei, j)
            double t = 2 * m_A[j] * m_A[vei];
            m_deltaQ[vei].emplace(std::make_pair(vei, j), deltaMvi - t);

            // Calculem un deltaM per (j, vei)
            double deltaMjv = m_deltaQ[j][std::make_pair(j, vei)] - t;
            m_deltaQ[j].emplace(std::make_pair(j, vei), deltaMjv);
        }
    }

    // els veins només de j
    for (int vei : vj) {
        double t = 2 * m_A[i] * m_A[vei];
        m_deltaQ[vei][std::make_pair(vei, j)] -= t;
        m_deltaQ[j][std::make_pair(j, vei)] -= t;
    }

    // Esborrem els elements del mapa m_deltaQ[i]
    m_deltaQ[i].clear();
    
    // Guardem el deltaQ per (j, i) i l'esborrem de m_deltaQ
    std::map<std::pair<int, int>, double>::iterator it 
        = m_deltaQ[j].find(std::make_pair(j, i));
    //double deltaQji = it->second;
    m_deltaQ[j].erase(it);

    // Modificar m_maxDeltaQFil i hTotal con los vectores
    std::vector<int> v;

    //for (int i : vi2)
    //    if (i != j)
    //        v.push_back(i);
    for (int i : vi)
        if (i != j)
            v.push_back(i);
    v.insert(v.end(), vij.begin(), vij.end());
    v.insert(v.end(), vj.begin(), vj.end());
    v.push_back(j);

    for (int i : v) {
        std::pair<int, double> maxDeltaQi = std::make_pair(-1, -1);
        for (std::pair<std::pair<int, int>, double> p : m_deltaQ[i]) {
            int j = p.first.second;
            if (p.second >= maxDeltaQi.second)
                maxDeltaQi = std::make_pair(j, p.second);
        }
        m_maxDeltaQFil[i] = maxDeltaQi;
        m_hTotal.modifElem(ElemHeap(maxDeltaQi.second, std::make_pair(i, maxDeltaQi.first)));
    }

    // Actualizem el vector de comunitats actives i la primera comunitat activa
    // si escau (si borrem aquesta amb la fusió)
    if (i == m_primComdeltaQ)
        m_primComdeltaQ = m_indexComs[i].first;
    else
        m_indexComs[m_indexComs[i].second].first = m_indexComs[i].first;

    if (i != m_indexComs.size())
        m_indexComs[m_indexComs[i].first].second = m_indexComs[i].second;

    // Fusionem el arbres i-j del vector que representa el dendrograma
    Tree<double>* tj = m_vDendrograms[j];
    Tree<double>* ti = m_vDendrograms[i];
    
    m_Q += deltaQij;
    Tree<double>* tp = new Tree<double>(m_Q);
    tp->setLeft(tj);
    tp->setRight(ti);

    m_vDendrograms[i] = nullptr;
    m_vDendrograms[j] = tp;

    // Afegim el grau normalitzat de la comunitat i a la j
    m_A[j] += m_A[i];
}

void Comunitat::calculaComunitats(list<Tree<double>*>& listDendrogram)
{
    // Creamos las estructuras de datos
    calculaM2();
    calculaK();
    calculaA();
    creaDeltaQHeap();
    creaIndexComs();
	inicialitzaDendrograms();
    
    // Iteramos mientras queden comunidades por fusionar y el delta de la
    // fusión sea positiva
    while (m_indexComs[m_primComdeltaQ].first != m_indexComs.size() 
           && m_hTotal.max().getVal() > 0.0) {
        std::pair<int, int> pij = m_hTotal.max().getPos();
        
        m_hTotal.delMax();
        fusiona(pij.first, pij.second);
    }

    // Devolvemos la lista con el dendrograma
    for (Tree<double>* t : m_vDendrograms)
        if (t)
            listDendrogram.push_back(t);
}
