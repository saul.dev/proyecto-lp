#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <algorithm>
#include "Tree.hpp"

class MatriuSparse
{
    private:
        int files; 
        int columnes;

        std::vector<int> ia;
        std::vector<int> ja;
        std::vector<float> a;
        
        void resize(int files, int columnes) {
            // las matrices tienen que ser cuadradas siempre por alguna razón...
            int max = std::max(files, columnes);
            this->files = max;
            this->columnes = max;

            if (ia.size())
                ia.resize(files + 1, ia.back());
            else
                ia.resize(files + 1, 0);
        }

    public:
        MatriuSparse();
        MatriuSparse(int files, int columnes);
        MatriuSparse(const MatriuSparse& ms);
        MatriuSparse(const std::string& f);
        ~MatriuSparse();

        void init(int files, int columnes);

        int getNFiles() const;
        int getNColumnes() const;
        int getNValues() const;
        bool getVal(int fila, int columna, float& valor) const;
        void setVal(int fila, int columna, float valor);

        void calculaGrau(vector<int>& graus) const;
        void creaMaps(vector<map<pair<int, int>, double>>& vMaps) const;
        void calculaDendograms(vector<Tree<double>*>& vDendograms) const;
        void clear();

        MatriuSparse& operator =(const MatriuSparse& ms);
        MatriuSparse operator *(float k) const;
        std::vector<float> operator *(const std::vector<float>& v) const;
        MatriuSparse operator /(float k) const;
        friend std::ostream& operator <<(std::ostream& out, const MatriuSparse& ms);
};
